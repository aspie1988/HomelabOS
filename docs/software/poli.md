# Poli

[Poli](https://github.com/shzlw/poli) An easy-to-use BI server built for SQL lovers. Power data analysis in SQL and gain faster business insights.

## Access

It is available at [https://poli.{{ domain }}/](https://poli.{{ domain }}/) or [http://poli.{{ domain }}/](http://poli.{{ domain }}/)

{% if enable_tor %}
It is also available via Tor at [http://poli.{{ tor_domain }}/](http://poli.{{ tor_domain }}/)
{% endif %}
